package com.gptravel.converter;

import com.vaadin.data.Converter;
import com.vaadin.data.Result;
import com.vaadin.data.ValueContext;

import java.time.Duration;
import java.time.LocalDate;

public  class LocalDateConverter implements Converter<LocalDate, Long> {

    @Override
    public Result<Long> convertToModel(LocalDate localDate, ValueContext valueContext) {
        if(localDate == null) {
            Result.ok(null);
        }
        long countDays = Duration.between(localDate.atTime(0,0), LocalDate.now().atTime(0,0)).toDays();
        return Result.ok(countDays);
    }

    @Override
    public LocalDate convertToPresentation(Long aLong, ValueContext valueContext) {
        if(aLong == null) {
            return LocalDate.now();
        }
        return LocalDate.now().minusDays(aLong);
    }
}
