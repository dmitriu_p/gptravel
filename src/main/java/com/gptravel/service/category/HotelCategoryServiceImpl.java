package com.gptravel.service.category;


import com.gptravel.dao.HotelCategoryRepository;
import com.gptravel.entity.HotelCategory;
import com.gptravel.service.hotel.HotelServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class HotelCategoryServiceImpl implements HotelCategoryService {

    private static final Logger LOGGER = Logger.getLogger(HotelCategoryServiceImpl.class.getName());



    @PersistenceContext(unitName = "hotel_demo", type = PersistenceContextType.TRANSACTION)
    private EntityManager entityManager;


    @Override
    public HotelCategory create(HotelCategory hotelCategory) {
        return null;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(HotelCategory hotelCategory) {
        HotelCategory rem = entityManager.find(HotelCategory.class, hotelCategory.getId());
        entityManager.remove(rem);
        entityManager.flush();

    }

    @Transactional
    @Override
    public HotelCategory getById(Long id) {
        return entityManager.find(HotelCategory.class, id);
    }

    @Transactional
    @Override
    public void update(HotelCategory hotelCategory) {
        if (hotelCategory == null) {
            LOGGER.log(Level.SEVERE, "Category is null.");
            return;
        }
        HotelCategory category = entityManager.merge(hotelCategory);
        entityManager.persist(category);
    }

    @Override
    public List<HotelCategory> findAll() {
        return entityManager.createQuery("select c from HotelCategory c", HotelCategory.class).getResultList();
    }

    public List<HotelCategory> findAll(String filter, int start, int maxResults) {
        return entityManager.createQuery("select c from HotelCategory c", HotelCategory.class).setFirstResult(start)
                .setMaxResults(maxResults).getResultList();
    }
}
