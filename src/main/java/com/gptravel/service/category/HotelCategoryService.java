package com.gptravel.service.category;


import com.gptravel.entity.HotelCategory;
import org.springframework.stereotype.Service;

import java.util.List;


public interface HotelCategoryService {

    HotelCategory create(HotelCategory hotelCategory);
    void delete(HotelCategory hotelCategory);
    HotelCategory getById(Long id);
    void update(HotelCategory hotelCategory);
    List<HotelCategory> findAll();
}


