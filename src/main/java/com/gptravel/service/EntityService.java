package com.gptravel.service;

import com.gptravel.service.category.HotelCategoryService;
import com.gptravel.service.category.HotelCategoryServiceImpl;
import com.gptravel.service.hotel.HotelService;
import com.gptravel.service.hotel.HotelServiceImpl;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class EntityService implements ApplicationContextAware {

	private static ApplicationContext appContext;

	@Override
	@Autowired
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		appContext = applicationContext;
	}

	public static HotelService getHotelService() {
		return appContext.getBean("hotelServiceImpl", HotelService.class);
	}

	public static HotelCategoryService getCategoryService() {
		return appContext.getBean("hotelCategoryServiceImpl", HotelCategoryService.class);
	}

}
