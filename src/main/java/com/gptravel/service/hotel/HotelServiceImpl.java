package com.gptravel.service.hotel;


import com.gptravel.dao.HotelCategoryRepository;
import com.gptravel.dao.HotelRepository;
import com.gptravel.entity.Hotel;
import com.gptravel.entity.HotelCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.*;
import java.util.logging.Logger;

@Service
public class HotelServiceImpl implements HotelService {

	private static final String SELECT_WITH_FILTER = "select h from Hotel h where lower(h.name) like :filterName or lower(h.address) like :filterAdr";
	private static final String SELECT_WITH_NAME = "select h from Hotel h where lower(h.name) like :filterName";
	private static final String SELECT_WITH_ADDRESS = "select h from Hotel h where lower(h.address) like :filterAdr";


	@PersistenceContext(unitName = "hotel_demo", type = PersistenceContextType.TRANSACTION)
	private EntityManager entityManager;


	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Hotel create(Hotel hotel) {
		return null;
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void delete(Hotel value) {
		Hotel hotel = entityManager.find(Hotel.class, value.getId());
		entityManager.remove(hotel);

	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public Hotel getById(long id) {
		return entityManager.find(Hotel.class, id);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void update(Hotel hotelEntry) {
		Hotel hotel = entityManager.merge(hotelEntry);
		entityManager.persist(hotel);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public List<Hotel> findAll() {
		return entityManager.createQuery("SELECT h from Hotel h", Hotel.class).getResultList();
	}

	@Transactional
	public List<Hotel> findAll(String filterName, String filterAdr) {
		if(filterName == "") {
			filterName = null;
		}
		if(filterAdr == "") {
			filterAdr = null;
		}

		if (filterName == null && filterAdr == null) {
			return findAll();
		}
		System.out.println(filterAdr == null);
		System.out.println(filterAdr == "");
		if(filterAdr == null) {
			return entityManager.createQuery(SELECT_WITH_NAME, Hotel.class)
					.setParameter("filterName", "%" + filterName.toLowerCase() + "%")
					.getResultList();
		} else if(filterName == null)  {
			return entityManager.createQuery(SELECT_WITH_ADDRESS, Hotel.class).
					setParameter("filterAdr", "%" + filterAdr.toLowerCase() + "%")
					.getResultList();
		} else {
			return entityManager.createQuery(SELECT_WITH_FILTER, Hotel.class)
					.setParameter("filterName", "%" + filterName.toLowerCase() + "%")
					.setParameter("filterAdr", "%" + filterAdr.toLowerCase() + "%")
					.getResultList();
		}

	}
}
