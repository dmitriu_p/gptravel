package com.gptravel.service.hotel;


import com.gptravel.entity.Hotel;
import org.springframework.stereotype.Service;

import java.util.List;



public interface HotelService {

    Hotel create(Hotel hotel);
    void delete(Hotel value);
    Hotel getById(long id);
    void update(Hotel hotel);
    List<Hotel> findAll();
    List<Hotel> findAll(String filterName, String filterAdr);
}
