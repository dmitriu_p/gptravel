package com.gptravel.form;

import com.gptravel.entity.HotelCategory;
import com.gptravel.service.EntityService;
import com.gptravel.service.category.HotelCategoryService;
import com.gptravel.service.category.HotelCategoryServiceImpl;
import com.gptravel.view.HotelCategoryView;
import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@UIScope
@SpringComponent
public class HotelCategoryForm extends BaseForm<HotelCategory> {

    private TextField name = new TextField("Name");
    private HotelCategoryService service;

    private HotelCategory hotelCategory;
    private HotelCategoryView hotelCategoryView;

    private Binder<HotelCategory> binder = new Binder<>(HotelCategory.class);

    public HotelCategoryForm(HotelCategoryView hotelCategoryView) {
        super();
        this.hotelCategoryView = hotelCategoryView;
        this.service = EntityService.getCategoryService();

        setSizeUndefined();
        addComponents(name, formButtons);

        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        //field's tooltips
        name.setDescription("Title of the category");

        bindFields();
    }

    public void bindFields() {
        binder.forField(name).asRequired("Please enter name").bind(HotelCategory::getHotelCategory, HotelCategory::setHotelCategory);

    }

    public void setCategoryHotel(HotelCategory hotelCategory) {
        this.hotelCategory = hotelCategory;
        binder.readBean(hotelCategory);
        setVisible(true);
        name.selectAll();
    }

    public void save() {
        boolean validCategory = binder.writeBeanIfValid(hotelCategory);
        if(validCategory) {
            EntityService.getCategoryService().update(hotelCategory);
            hotelCategoryView.updateList();
            cancelForm();
        } else {
            Notification.show("Invalid data");
        }
    }


}
