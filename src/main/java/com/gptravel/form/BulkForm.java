package com.gptravel.form;


import com.gptravel.converter.LocalDateConverter;
import com.gptravel.entity.Hotel;
import com.gptravel.entity.HotelCategory;
import com.gptravel.service.EntityService;
import com.gptravel.service.category.HotelCategoryService;
import com.gptravel.service.hotel.HotelService;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.selection.MultiSelectionEvent;
import com.vaadin.event.selection.MultiSelectionListener;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.HashSet;
import java.util.Set;

public class BulkForm extends BaseForm<Hotel> {
    private HotelService hotelService = EntityService.getHotelService();
    private HotelCategoryService categoryService = EntityService.getCategoryService();
    
    
    private Label label = new Label("Bulk update");
    private ComboBox<String> fieldsSelect = new ComboBox<>();
    private TextField defaultBulkField = new TextField();
    private TextField name = new TextField();
    private TextField address = new TextField();
    private TextField rating = new TextField();
    private NativeSelect<HotelCategory> category = new NativeSelect<>();
    private DateField operatesFrom = new DateField();
    private TextField url = new TextField("Url");
    private TextField description = new TextField();
   
    private Binder<Hotel> binderMain;
    private Binder<Hotel> binderName = new Binder<>(Hotel.class);
    private Binder<Hotel> binderAddress = new Binder<>(Hotel.class);
    private Binder<Hotel> binderRating = new Binder<>(Hotel.class);
    private Binder<Hotel> binderCategory = new Binder<>(Hotel.class);
    private Binder<Hotel> binderOperatesFrom = new Binder<>(Hotel.class);
    private Binder<Hotel> binderUrl = new Binder<>(Hotel.class);
    private Binder<Hotel> binderDescription = new Binder<>(Hotel.class);
    private Binder<Hotel> binderDefault = new Binder<>(Hotel.class);

    private static final String[] NAME_FIELD = new String[]{"Name", "Address", "Rating", "Category", "Operates from", "URL", "Description"};

    private Grid grid;
    private PopupView popupView;

    public BulkForm(Grid<Hotel> grid) {
        super();
        bindFields();
        this.grid = grid;

        defaultBulkField.setPlaceholder("Input value");
        defaultBulkField.setEnabled(false);

        category.setItems(EntityService.getCategoryService().findAll());
        category.addFocusListener(e -> category.setItems(EntityService.getCategoryService().findAll()));

        fieldsSelect.setPlaceholder("Please select field");
        fieldsSelect.setItems(NAME_FIELD);
        fieldsSelect.setTextInputAllowed(false);
        fieldsSelect.setEmptySelectionCaption("No field selected");
        fieldsSelect.clear();
        fieldsSelect.addValueChangeListener(event -> {
            String s = event.getValue();
            if (s == null) {
                defaultBulkField.clear();
                replaceComponent(getComponent(2), defaultBulkField);
            } else {
                switch (s) {
                    case "Name": {
                        replaceComponent(getComponent(2), name);
                        binderMain = binderName;
                        break;
                    }
                    case "Address": {
                        replaceComponent(getComponent(2), address);
                        binderMain = binderAddress;
                        break;
                    }
                    case "Rating": {
                        replaceComponent(getComponent(2), rating);
                        binderMain = binderRating;
                        break;
                    }
                    case "Category": {
                        replaceComponent(getComponent(2), category);
                        binderMain = binderCategory;
                        break;
                    }
                    case "Operates from": {
                        replaceComponent(getComponent(2), operatesFrom);
                        binderMain = binderOperatesFrom;
                        break;
                    }
                    case "URL": {
                        replaceComponent(getComponent(2), url);
                        binderMain = binderUrl;
                        break;
                    }
                    case "Description": {
                        replaceComponent(getComponent(2), description);
                        binderMain = binderDescription;
                        break;
                    }
                    default: {
                        replaceComponent(getComponent(2), defaultBulkField);
                        binderMain = binderDefault;
                    }
                }
            }
        });

        HorizontalLayout horizontalLayout = new HorizontalLayout(formButtons);
        fieldsSelect.setWidth(11, Unit.EM);
        setWidth(20, Unit.EM);
        addComponents(label, fieldsSelect, defaultBulkField, horizontalLayout);
        setEnabled(false);
    }

    private void removeBulkFields() {
        defaultBulkField.clear();
        name.clear();
        address.clear();
        rating.clear();
        category.clear();
        url.clear();
        description.clear();
    }

    public void cancelForm() {
        binder.removeBean();
        popupView.setPopupVisible(false);
    }

    @Override
    protected void save() {
        Set<Hotel> selected = grid.getSelectedItems();
        selected.forEach(item -> EntityService.getHotelService().update(item));
        fieldsSelect.clear();
        this.cancelForm();

    }

    public void bindFields() {
        binder.forField(rating).withConverter(new StringToIntegerConverter(0, "Only digits!"))
                .withValidator(v -> v < 6, "Rating should be a positive number less then 6")
                .withValidator(v -> v > 0, "Rating should be a positive")
                .asRequired("Please enter rating!")
                .bind(Hotel::getRating, Hotel::setRating);
        binder.forField(name).asRequired("Please enter name").bind(Hotel::getName, Hotel::setName);
        binder.forField(address).asRequired("Please enter address").bind(Hotel::getAddress, Hotel::setAddress);
        binder.forField(operatesFrom).withConverter(new LocalDateConverter())
                .withValidator(d -> d >= 0, "Date should be before today's")
                .asRequired("Please enter date")
                .bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
        binder.forField(category).asRequired("Please choose category!").bind(Hotel::getCategory, Hotel::setCategory);
        binder.forField(url).asRequired("Please enter correct url").bind(Hotel::getUrl, Hotel::setUrl);
        binder.forField(description).bind(Hotel::getDescription, Hotel::setDescription);
//        binder.bindInstanceFields(this);
    }

    public void setPopupView(PopupView popupView) {
        this.popupView = popupView;
        Set<Hotel> selected = grid.getSelectedItems();
        this.popupView.addPopupVisibilityListener(event -> {
            removeBulkFields();
            if (!popupView.isPopupVisible()) {
                selected.forEach(item -> EntityService.getHotelService().update(item));
                grid.setItems(EntityService.getHotelService().findAll());
                grid.deselectAll();
                removeBulkFields();
                fieldsSelect.clear();
            }
        });
    }




}
