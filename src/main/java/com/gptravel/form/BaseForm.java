package com.gptravel.form;


import com.vaadin.data.Binder;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SuppressWarnings("serial")
public abstract class BaseForm<T> extends FormLayout  {

    protected Button save = new Button("Save");
    protected Button cancel = new Button("Cancel");
    HorizontalLayout formButtons = new HorizontalLayout(save, cancel);
    Binder<T> binder = new Binder<>();

    public BaseForm() {
        setSizeUndefined();
        
        save.setStyleName(ValoTheme.BUTTON_PRIMARY);
        save.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        
        save.addClickListener(e -> save());
        cancel.addClickListener(e -> cancelForm());
    }

    public void cancelForm() {
        binder.removeBean();
        this.setVisible(false);
    }

    protected abstract void save();
    protected abstract void bindFields();

}
