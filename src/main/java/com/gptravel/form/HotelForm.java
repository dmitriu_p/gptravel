package com.gptravel.form;

import com.gptravel.converter.LocalDateConverter;
import com.gptravel.entity.Hotel;
import com.gptravel.entity.HotelCategory;
import com.gptravel.service.EntityService;
import com.gptravel.service.category.HotelCategoryService;
import com.gptravel.service.category.HotelCategoryServiceImpl;
import com.gptravel.service.hotel.HotelService;
import com.gptravel.view.HotelView;
import com.vaadin.data.Binder;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@UIScope
@SpringComponent
public class HotelForm extends BaseForm<Hotel> {

    private TextField name = new TextField("Name");
    private TextField address = new TextField("Address");
    private TextField rating = new TextField("Rating");
    private DateField operatesFrom = new DateField("Operates From");
    private NativeSelect<HotelCategory> category = new NativeSelect<>("Category");
    private TextField url = new TextField("URL");
    private TextArea description = new TextArea("Description");
    private static final long serialVersionUID = -2249768301112428822L;

    private HotelService hotelServiceImpl;

    private HotelCategoryService categoryService;

    private Hotel hotel;
    private HotelView hotelView;
    private Binder<Hotel> binder = new Binder<>(Hotel.class);

    private Button save = new Button("Save");

    private Button delete = new Button("Delete");


    public HotelForm(HotelView hotelView) {
        super();
        this.hotelView = hotelView;
        this.hotelServiceImpl = EntityService.getHotelService();
        this.categoryService = EntityService.getCategoryService();

        setSizeUndefined();
        addComponents(name, address, rating, operatesFrom, category, url, description, formButtons);

        category.setItems(EntityService.getCategoryService().findAll());
        category.setItemCaptionGenerator(item -> item.getHotelCategory());
        category.setWidth(11, Unit.EM);

        //field's tooltips
        name.setDescription("Title of the hotel");
        address.setDescription("Location of the hotel");
        rating.setDescription("Rate the hotel");
        operatesFrom.setDescription("Indicate the opening of the hotel");
        category.setDescription("Choose type of place");
        url.setDescription("Link to hotel site");
        description.setDescription("Your impressions of the hotel");
        bindFields();
    }

    public void bindFields() {
        binder.forField(rating).withConverter(new StringToIntegerConverter(0, "Only digits!"))
                .withValidator(v -> v < 6, "Rating should be a positive number less then 6")
                .withValidator(v -> v > 0, "Rating should be a positive")
                .asRequired("Please enter rating!")
                .bind(Hotel::getRating, Hotel::setRating);
        binder.forField(name).asRequired("Please enter name").bind(Hotel::getName, Hotel::setName);
        binder.forField(address).asRequired("Please enter address").bind(Hotel::getAddress, Hotel::setAddress);
        binder.forField(operatesFrom).withConverter(new LocalDateConverter())
                .withValidator(d -> d >= 0, "Date should be before today's")
                .asRequired("Please enter date")
                .bind(Hotel::getOperatesFrom, Hotel::setOperatesFrom);
        binder.forField(category).asRequired("Please choose category!").bind(Hotel::getCategory, Hotel::setCategory);
        binder.forField(url).asRequired("Please enter correct url").bind(Hotel::getUrl, Hotel::setUrl);
        binder.forField(description).bind(Hotel::getDescription, Hotel::setDescription);
//        binder.bindInstanceFields(this);
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
        binder.readBean(hotel);
        cancel.setVisible(hotel.isPersisted());
        setVisible(true);
    }

    public void save() {
        boolean validHotel = binder.writeBeanIfValid(hotel);
        if(validHotel) {
            EntityService.getHotelService().update(hotel);
                hotelView.updateList();
                setVisible(false);
                cancelForm();
        } else {
            Notification.show("Invalid data");
        }
    }
}