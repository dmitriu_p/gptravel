package com.gptravel.main;

import com.gptravel.service.EntityService;
import com.gptravel.service.category.HotelCategoryService;
import com.gptravel.view.HotelCategoryView;
import com.gptravel.view.HotelView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

import static com.gptravel.constant.Constants.CATEGORY_VIEW;
import static com.gptravel.constant.Constants.HOTEL_VIEW;


@SuppressWarnings("serial")
@Theme("mytheme")
@SpringUI
public class GPTravelUI extends UI {

    public static Navigator navigator;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        navigator = new Navigator(this, this);

        navigator.addView(HOTEL_VIEW, new HotelView());
        navigator.addView(CATEGORY_VIEW, new HotelCategoryView());
    }


    @WebServlet(urlPatterns = "/*", name = "GPTravelUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = GPTravelUI.class, productionMode = false)
    public static class GPTravelUIServlet extends SpringVaadinServlet {
    }

    @WebListener
    public static class MyContextLoaderListener extends ContextLoaderListener {}

}
