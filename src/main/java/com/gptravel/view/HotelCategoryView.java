package com.gptravel.view;

import com.gptravel.entity.Hotel;
import com.gptravel.entity.HotelCategory;
import com.gptravel.form.HotelCategoryForm;
import com.gptravel.form.HotelForm;
import com.gptravel.service.EntityService;
import com.gptravel.service.category.HotelCategoryService;
import com.gptravel.service.category.HotelCategoryServiceImpl;
import com.vaadin.annotations.Title;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.components.grid.MultiSelectionModel;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.vaadin.viritin.util.HtmlElementPropertySetter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Title("HotelCategory")
@SuppressWarnings("serial")
@SpringView
public class HotelCategoryView extends BaseView  {


    private Grid<HotelCategory> gridCategory = new Grid<>(HotelCategory.class);
    private HotelCategoryForm categoryForm = new HotelCategoryForm(this);

    public HotelCategoryView() {
        addComponents(new HotelMenu());
        categoryForm = new HotelCategoryForm(this);

        initGridComponent();
        categoryForm.setVisible(false);
        HorizontalLayout controlPannel = new HorizontalLayout();
        initControls(controlPannel);

        HorizontalLayout main = new HorizontalLayout(gridCategory, categoryForm);
        main.setSizeFull();
        main.setExpandRatio(gridCategory, 1);

        addComponents(controlPannel, main);
    }

    private void initControls(HorizontalLayout controlPannel) {

        HorizontalLayout buttons = getButtons();
        controlPannel.addComponents(buttons);
    }

    private void initGridComponent() {
        gridCategory.setColumns("hotelCategory");
        gridCategory.setSelectionMode(Grid.SelectionMode.MULTI);

        gridCategory.addSelectionListener(e -> {
            int selected = e.getAllSelectedItems().size();
            enableButtons(selected == 1, selected > 0);
                categoryForm.cancelForm();

        });
        gridCategory.setSizeFull();
        gridCategory.setHeight(31, Unit.EM);

        updateList();
    }

    @Override
    protected void delete() {
        gridCategory.getSelectedItems().stream().forEach(i -> EntityService.getCategoryService().delete(i));
        Notification not = new Notification("Category sucessfully deleted!");
        not.show(UI.getCurrent().getPage());
        updateList();
    }

    @Override
    protected void edit() {
        categoryForm.setVisible(true);
        categoryForm.setCategoryHotel(gridCategory.getSelectedItems().iterator().next());
    }

    @Override
    protected void add() {
        HotelCategory hotelCategory = new HotelCategory();
        categoryForm.setVisible(true);
        categoryForm.setCategoryHotel(hotelCategory);
    }

    public void updateList() {
        gridCategory.setItems(EntityService.getCategoryService().findAll());
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

}
