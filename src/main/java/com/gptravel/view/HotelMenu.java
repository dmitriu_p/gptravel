package com.gptravel.view;

import com.gptravel.main.GPTravelUI;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.MenuBar;
import org.springframework.stereotype.Component;

import static com.gptravel.constant.Constants.CATEGORY_VIEW;
import static com.gptravel.constant.Constants.HOTEL_VIEW;

@SuppressWarnings("serial")
@UIScope
@SpringComponent
public class HotelMenu extends CssLayout {

    public HotelMenu() {

        MenuBar hotelMenu = new MenuBar();
        addComponent(hotelMenu);
        MenuBar.MenuItem hotelList = hotelMenu.addItem("Hotel List", e -> {
                    GPTravelUI.navigator.navigateTo(HOTEL_VIEW);
    });

        MenuBar.MenuItem hotelCategory = hotelMenu.addItem("Hotel Category", e -> {
            GPTravelUI.navigator.navigateTo(CATEGORY_VIEW);
        });
    }
}
