package com.gptravel.view;

import com.gptravel.dao.HotelCategoryRepository;
import com.gptravel.dao.HotelRepository;
import com.gptravel.entity.Hotel;
import com.gptravel.entity.HotelCategory;
import com.gptravel.form.BulkForm;
import com.gptravel.form.HotelForm;
import com.gptravel.service.EntityService;
import com.gptravel.service.hotel.HotelService;
import com.gptravel.service.hotel.HotelServiceImpl;
import com.vaadin.annotations.Title;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.*;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.themes.ValoTheme;
import org.vaadin.viritin.util.HtmlElementPropertySetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

@Title("Hotel")
@SuppressWarnings("serial")
@SpringView
public class HotelView extends BaseView {


    private Grid<Hotel> grid = new Grid<>(Hotel.class);
    private TextField filterName = new TextField();
    private TextField filterAddress = new TextField();
    private Button bulkUpdateHotel = new Button("Bulk update", VaadinIcons.ABACUS);

    private BulkForm hotelBulkForm = new BulkForm(grid);
    private PopupView popup = new PopupView(null, hotelBulkForm);

    private HotelForm hotelForm = new HotelForm(this);

    ListDataProvider<Hotel> dataProvider;

    public HotelView() {

        addComponents(new HotelMenu());
        hotelForm = new HotelForm(this);
        HorizontalLayout controlPannel = new HorizontalLayout();
        initControls(controlPannel);

        initGridComponent();
        hotelForm.setVisible(false);

        HorizontalLayout main = new HorizontalLayout(grid, hotelForm);
        main.setSizeFull();
        main.setExpandRatio(grid, 1);

        addComponents(controlPannel, main, popup);

    }

    public void updateList() {
        List<Hotel> hotels = EntityService.getHotelService().findAll(filterName.getValue(), filterAddress.getValue());
        grid.setItems(hotels);
    }

    private void initControls(HorizontalLayout controlPannel) {
        //filter by name
        filterName.setPlaceholder("filter by name...");
        filterName.addValueChangeListener(e -> updateList());
        filterName.setValueChangeMode(ValueChangeMode.LAZY);
        filterName.setStyleName(ValoTheme.TEXTFIELD_TINY);
        //filter by address
        filterAddress.setPlaceholder("filter by address...");
        filterAddress.addValueChangeListener(e -> updateList());
        filterAddress.setValueChangeMode(ValueChangeMode.LAZY);
        filterAddress.setStyleName(ValoTheme.TEXTFIELD_TINY);

        HtmlElementPropertySetter sName = new HtmlElementPropertySetter(filterName);
        HtmlElementPropertySetter sAdr = new HtmlElementPropertySetter(filterAddress);
        sName.setProperty("type", "search");
        sAdr.setProperty("type", "search");

        bulkUpdateHotel.setStyleName(ValoTheme.BUTTON_TINY);
        hotelBulkForm.setPopupView(popup);
        popup.setHideOnMouseOut(false);

        bulkUpdateHotel.setEnabled(true);
        bulkUpdateHotel.addClickListener(event -> {
            hotelForm.cancelForm();
            popup.setPopupVisible(true);
        });

        HorizontalLayout buttons = getButtons();
        controlPannel.addComponents(filterName, filterAddress, bulkUpdateHotel,  buttons);
    }

    private void initGridComponent() {
        grid.setColumns("name", "rating", "address");
        grid.addColumn(hotel -> hotel.getCategory() == null ? "Not defined" : hotel.getCategory().getHotelCategory(),
                new HtmlRenderer()).setCaption("Category");
        grid.addColumn(hotel -> "<a href='" + hotel.getUrl() + "' target='_blank'>Go to site</a>", new HtmlRenderer())
                .setCaption("Link").setId("url");

        grid.setSelectionMode(Grid.SelectionMode.MULTI);

        grid.addSelectionListener(e -> {
            int selected = e.getAllSelectedItems().size();
            enableButtons(selected == 1, selected > 0);
            bulkUpdateHotel.setEnabled(selected > 0);
        });
        grid.setSizeFull();
        grid.setHeight(31, Unit.EM);

        updateList();
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
    }

    @Override
    protected void delete() {
        Set<Hotel> selected = grid.getSelectedItems();
        selected.forEach(item -> EntityService.getHotelService().delete(item));
        hotelForm.cancelForm();
        updateList();
    }

    @Override
    protected void edit() {
        hotelForm.setVisible(true);
        hotelForm.setHotel(grid.getSelectedItems().iterator().next());
    }

    @Override
    protected void add() {
        Hotel h = new Hotel();
        hotelForm.setVisible(true);
        hotelForm.setHotel(h);

    }
}
