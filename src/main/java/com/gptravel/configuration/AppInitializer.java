package com.gptravel.configuration;

import com.vaadin.spring.server.SpringVaadinServlet;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

//public class AppInitializer implements WebApplicationInitializer {
//    @Override
//    public void onStartup(javax.servlet.ServletContext servletContext)
//            throws ServletException {
//        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
//        context.scan(AppInitializer.class.getPackage().getName());
//        servletContext.addListener(new ContextLoaderListener(context));
//        registerServlet(servletContext);
//    }
//
//    private void registerServlet(ServletContext servletContext) {
//        ServletRegistration.Dynamic dispatcher = servletContext.addServlet(
//                "vaadin", SpringVaadinServlet.class);
//        dispatcher.setLoadOnStartup(1);
//        dispatcher.addMapping("/*");
//    }
//}
