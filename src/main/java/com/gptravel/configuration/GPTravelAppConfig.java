package com.gptravel.configuration;

import com.gptravel.service.EntityService;
import com.vaadin.spring.annotation.EnableVaadin;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableVaadin
@ComponentScan(basePackages = "com.gptravel.service")
public class GPTravelAppConfig {

    @Bean
    public static EntityService getEntityService() {
        return new EntityService();
    }

}
