package com.gptravel.dao;


import com.gptravel.entity.HotelCategory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HotelCategoryRepository extends JpaRepository<HotelCategory, Long> {

    List<HotelCategory> findAllBy(Pageable pageable);
}
