package com.gptravel.entity;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@SuppressWarnings("serial")
@Entity
@Table(name="CATEGORY")
public class HotelCategory implements Serializable, Cloneable {

    @Id
    @Column(name="CATEGORY_ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Version
    @Column(name="OPTLOCK")
    private Long version;

    @Column(name = "NAME")
    @Size(max = 255)
    public String hotelCategory = "";

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return hotelCategory;
    }

    public boolean isPersisted() {
        return id != null;
    }

    @Override
    public HotelCategory clone() throws CloneNotSupportedException {
        return (HotelCategory) super.clone();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getHotelCategory() {
        return hotelCategory;
    }

    public void setHotelCategory(String hotelCategory) {
        this.hotelCategory = hotelCategory;
    }

    public HotelCategory(Long id, String hotelCategory) {
        this.id = id;
        this.hotelCategory = hotelCategory;
    }

    public HotelCategory() {
    }

    public HotelCategory(String hotelCategory) {
        this.hotelCategory = hotelCategory;
    }

}
